package com.amazonws.rest.kinesisDemo.controller;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amazonws.rest.kinesisDemo.service.KinesisConsumerTest;
import com.amazonws.rest.kinesisDemo.service.KinesisProducerTest;

@RestController
public class KinesisController {
	
	@Resource(name = "kinesisConsumer")
	private KinesisConsumerTest consumer;
	
	@Resource(name = "kinesisProducer")
	private KinesisProducerTest producer;
	
	private Logger logger = LoggerFactory.getLogger(KinesisController.class);
	
	@PostMapping("/consume" )
	public String getMessages(@RequestBody String jumpKey) {
		
		logger.info("received key: {}", jumpKey);		
		String messages = StringUtils.EMPTY;
		String args[] = { "jump-stream-carlos", "us-west-2", jumpKey};
		try {
			messages = consumer.getKinesisMessages(args);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return messages;
	}
	
	@PostMapping("/produce")
	public void produceMessage(@RequestBody String consoleIn) {
		logger.info("received consoleIn: {}", consoleIn);
		producer.createStream("jump-stream-carlos", "us-west-2", consoleIn);
	}

}

package com.amazonws.rest.kinesisDemo.dto.json.streams;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "metadata", "category", "event", "jdk", "contextInformation", "approximateArrivalTimestamp" })
public class Event {

	@JsonProperty("metadata")
	private Event_Metadata metadata;
	@JsonProperty("category")
	private int category;
	@JsonProperty("event")
	private int event;
	@JsonProperty("jdk")
	private Event_Jdk jdk;
	@JsonProperty("contextInformation")
	private Event_ContextInformation contextInformation;
	@JsonProperty("approximateArrivalTimestamp")
	private long approximateArrivalTimestamp;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("metadata")
	public Event_Metadata getMetadata() {
		return metadata;
	}

	@JsonProperty("metadata")
	public void setMetadata(Event_Metadata metadata) {
		this.metadata = metadata;
	}

	@JsonProperty("category")
	public int getCategory() {
		return category;
	}

	@JsonProperty("category")
	public void setCategory(int category) {
		this.category = category;
	}

	@JsonProperty("event")
	public int getEvent() {
		return event;
	}

	@JsonProperty("event")
	public void setEvent(int event) {
		this.event = event;
	}

	@JsonProperty("jdk")
	public Event_Jdk getJdk() {
		return jdk;
	}

	@JsonProperty("jdk")
	public void setJdk(Event_Jdk jdk) {
		this.jdk = jdk;
	}
	
	@JsonProperty("contextInformation")
	public Event_ContextInformation getContextInformation() {
		return contextInformation;
	}

	@JsonProperty("contextInformation")
	public void setContextInformation(Event_ContextInformation contextInformation) {
		this.contextInformation = contextInformation;
	}

	@JsonProperty("approximateArrivalTimestamp")
	public long getApproximateArrivalTimestamp() {
		return approximateArrivalTimestamp;
	}

	@JsonProperty("approximateArrivalTimestamp")
	public void setApproximateArrivalTimestamp(long approximateArrivalTimestamp) {
		this.approximateArrivalTimestamp = approximateArrivalTimestamp;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
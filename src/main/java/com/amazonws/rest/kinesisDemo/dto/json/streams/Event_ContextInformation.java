package com.amazonws.rest.kinesisDemo.dto.json.streams;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "playerInfo" })
public class Event_ContextInformation {

	@JsonProperty("playerInfo")
	private Event_ContextInformation_PlayerInfo playerInfo;
	private Event_ContextInformation_ContentInfo contentInfo;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("playerInfo")
	public Event_ContextInformation_PlayerInfo getPlayerInfo() {
		return playerInfo;
	}

	@JsonProperty("contentInfo")
	public void setContentInfo(Event_ContextInformation_ContentInfo contentInfo) {
		this.contentInfo = contentInfo;
	}
	
	@JsonProperty("contentInfo")
	public Event_ContextInformation_ContentInfo getContentInfo() {
		return contentInfo;
	}

	@JsonProperty("playerInfo")
	public void setPlayerInfo(Event_ContextInformation_PlayerInfo playerInfo) {
		this.playerInfo = playerInfo;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
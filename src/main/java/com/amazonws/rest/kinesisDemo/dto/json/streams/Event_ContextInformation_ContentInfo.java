package com.amazonws.rest.kinesisDemo.dto.json.streams;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "contentTitle", "contentDescription" })

public class Event_ContextInformation_ContentInfo {

	@JsonProperty("contentTitle")
	private String contentTitle;
	@JsonProperty("contentDescription")
    private String contentDescription;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("contentTitle")
	public String getContentTitle() {
		return contentTitle;
	}

	@JsonProperty("contentTitle")
	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	@JsonProperty("contentDescription")
	public String getContentDescription() {
		return contentDescription;
	}

	@JsonProperty("contentDescription")
	public void setContentDescription(String contentDescription) {
		this.contentDescription = contentDescription;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
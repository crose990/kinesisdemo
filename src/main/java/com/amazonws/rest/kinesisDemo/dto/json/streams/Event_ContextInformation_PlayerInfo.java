package com.amazonws.rest.kinesisDemo.dto.json.streams;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "playbackSession", "contentId", "currentTime", "totalTime", "playerType", "contextData" })
public class Event_ContextInformation_PlayerInfo {

	@JsonProperty("playbackSession")
	private String playbackSession;
	@JsonProperty("contentId")
	private String contentId;
	@JsonProperty("currentTime")
	private int currentTime;
	@JsonProperty("totalTime")
	private int totalTime;
	@JsonProperty("playerType")
	private int playerType;
	@JsonProperty("contextData")
	private Event_ContextInformation_PlayerInfo_ContextData contextData;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("playbackSession")
	public String getPlaybackSession() {
		return playbackSession;
	}

	@JsonProperty("playbackSession")
	public void setPlaybackSession(String playbackSession) {
		this.playbackSession = playbackSession;
	}

	@JsonProperty("contentId")
	public String getContentId() {
		return contentId;
	}

	@JsonProperty("contentId")
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	@JsonProperty("currentTime")
	public int getCurrentTime() {
		return currentTime;
	}

	@JsonProperty("currentTime")
	public void setCurrentTime(int currentTime) {
		this.currentTime = currentTime;
	}

	@JsonProperty("totalTime")
	public int getTotalTime() {
		return totalTime;
	}

	@JsonProperty("totalTime")
	public void setTotalTime(int totalTime) {
		this.totalTime = totalTime;
	}

	@JsonProperty("playerType")
	public int getPlayerType() {
		return playerType;
	}

	@JsonProperty("playerType")
	public void setPlayerType(int playerType) {
		this.playerType = playerType;
	}

	@JsonProperty("contextData")
	public Event_ContextInformation_PlayerInfo_ContextData getContextData() {
		return contextData;
	}

	@JsonProperty("contextData")
	public void setContextData(Event_ContextInformation_PlayerInfo_ContextData contextData) {
		this.contextData = contextData;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
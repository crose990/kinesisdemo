package com.amazonws.rest.kinesisDemo.dto.json.streams;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "dateTime", "eventType", "userInfo", "deviceInfo", "JUMP-APP-KEY", "jumpAppKey" })
public class Event_Metadata {

	@JsonProperty("dateTime")
	private double dateTime;
	@JsonProperty("eventType")
	private int eventType;
	@JsonProperty("userInfo")
	private Event_Metadata_UserInfo userInfo;
	@JsonProperty("deviceInfo")
	private Event_Metadata_DeviceInfo deviceInfo;
	@JsonProperty("JUMP-APP-KEY")
	private String jUMPAPPKEY;
	@JsonProperty("jumpAppKey")
	private String jumpAppKey;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("dateTime")
	public double getDateTime() {
		return dateTime;
	}

	@JsonProperty("dateTime")
	public void setDateTime(double dateTime) {
		this.dateTime = dateTime;
	}

	@JsonProperty("eventType")
	public int getEventType() {
		return eventType;
	}

	@JsonProperty("eventType")
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}

	@JsonProperty("userInfo")
	public Event_Metadata_UserInfo getUserInfo() {
		return userInfo;
	}

	@JsonProperty("userInfo")
	public void setUserInfo(Event_Metadata_UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	@JsonProperty("deviceInfo")
	public Event_Metadata_DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@JsonProperty("deviceInfo")
	public void setDeviceInfo(Event_Metadata_DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	@JsonProperty("JUMP-APP-KEY")
	public String getJUMPAPPKEY() {
		return jUMPAPPKEY;
	}

	@JsonProperty("JUMP-APP-KEY")
	public void setJUMPAPPKEY(String jUMPAPPKEY) {
		this.jUMPAPPKEY = jUMPAPPKEY;
	}

	@JsonProperty("jumpAppKey")
	public String getJumpAppKey() {
		return jumpAppKey;
	}

	@JsonProperty("jumpAppKey")
	public void setJumpAppKey(String jumpAppKey) {
		this.jumpAppKey = jumpAppKey;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
package com.amazonws.rest.kinesisDemo.dto.json.streams;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "deviceUdid", "devicePlatform", "deviceModel", "deviceFormFactor", "screenWidth", "screenHeight",
		"IP" })
public class Event_Metadata_DeviceInfo {

	@JsonProperty("deviceUdid")
	private String deviceUdid;
	@JsonProperty("devicePlatform")
	private String devicePlatform;
	@JsonProperty("deviceModel")
	private String deviceModel;
	@JsonProperty("deviceFormFactor")
	private String deviceFormFactor;
	@JsonProperty("screenWidth")
	private String screenWidth;
	@JsonProperty("screenHeight")
	private String screenHeight;
	@JsonProperty("IP")
	private String IP;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("deviceUdid")
	public String getDeviceUdid() {
		return deviceUdid;
	}

	@JsonProperty("deviceUdid")
	public void setDeviceUdid(String deviceUdid) {
		this.deviceUdid = deviceUdid;
	}

	@JsonProperty("devicePlatform")
	public String getDevicePlatform() {
		return devicePlatform;
	}

	@JsonProperty("devicePlatform")
	public void setDevicePlatform(String devicePlatform) {
		this.devicePlatform = devicePlatform;
	}

	@JsonProperty("deviceModel")
	public String getDeviceModel() {
		return deviceModel;
	}

	@JsonProperty("deviceModel")
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	@JsonProperty("deviceFormFactor")
	public String getDeviceFormFactor() {
		return deviceFormFactor;
	}

	@JsonProperty("deviceFormFactor")
	public void setDeviceFormFactor(String deviceFormFactor) {
		this.deviceFormFactor = deviceFormFactor;
	}

	@JsonProperty("screenWidth")
	public String getScreenWidth() {
		return screenWidth;
	}

	@JsonProperty("screenWidth")
	public void setScreenWidth(String screenWidth) {
		this.screenWidth = screenWidth;
	}

	@JsonProperty("screenHeight")
	public String getScreenHeight() {
		return screenHeight;
	}

	@JsonProperty("screenHeight")
	public void setScreenHeight(String screenHeight) {
		this.screenHeight = screenHeight;
	}

	@JsonProperty("IP")
	public String getIP() {
		return IP;
	}

	@JsonProperty("IP")
	public void setIP(String iP) {
		this.IP = iP;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
package com.amazonws.rest.kinesisDemo.service;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder;
import com.amazonaws.services.kinesis.model.DescribeStreamRequest;
import com.amazonaws.services.kinesis.model.DescribeStreamResult;
import com.amazonaws.services.kinesis.model.GetRecordsRequest;
import com.amazonaws.services.kinesis.model.GetRecordsResult;
import com.amazonaws.services.kinesis.model.GetShardIteratorRequest;
import com.amazonaws.services.kinesis.model.GetShardIteratorResult;
import com.amazonaws.services.kinesis.model.Record;
import com.amazonaws.services.kinesis.model.ResourceNotFoundException;
import com.amazonaws.services.kinesis.model.Shard;
import com.amazonws.rest.kinesisDemo.dto.json.streams.Event;
import com.amazonws.rest.kinesisDemo.utils.ConfigurationUtils;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Continuously simulated Kinesis stream
 *
 */

@Component("kinesisConsumer")
public class KinesisConsumerTest {
	
	private static Logger logger = LoggerFactory.getLogger(KinesisConsumerTest.class);

	private static void checkUsage(String[] args) {
		if (args.length != 3) {
			logger.error("Usage: " + KinesisConsumerTest.class.getSimpleName() + " <stream name> <region> <Key>");
			throw new RuntimeException();
		}
	}

	/**
	 * Checks if the stream exists and is active
	 *
	 * @param kinesisClient
	 *            Amazon Kinesis client instance
	 * @param streamName
	 *            Name of stream
	 */
	private static void validateStream(AmazonKinesis kinesisClient, String streamName) {
		try {
			DescribeStreamResult result = kinesisClient.describeStream(streamName);
			if (!"ACTIVE".equals(result.getStreamDescription().getStreamStatus())) {
				logger.error("Stream " + streamName + " is not active. Please wait a few moments and try again.");
				throw new RuntimeException();
			}
		} catch (ResourceNotFoundException e) {
			logger.error("Stream " + streamName + " does not exist. Please create it in the console.");
			logger.error(e.getMessage());
			throw new RuntimeException();
		} catch (Exception e) {
			logger.error("Error found while describing the stream " + streamName);
			logger.error(e.getMessage());
			throw new RuntimeException();
		}
	}

	private static String getShardId(String streamName, AmazonKinesis kinesisClient) {
		DescribeStreamRequest describeStreamRequest = new DescribeStreamRequest();
		describeStreamRequest.setStreamName(streamName);
		List<Shard> shards = new ArrayList<>();
		String exclusiveStartShardId = null;
		describeStreamRequest.setExclusiveStartShardId(exclusiveStartShardId);
		DescribeStreamResult describeStreamResult = kinesisClient.describeStream(describeStreamRequest);
		shards.addAll(describeStreamResult.getStreamDescription().getShards());
		exclusiveStartShardId = shards.get(shards.size() - 1).getShardId();

		return exclusiveStartShardId;
	}

	public String getKinesisMessages(String[] args) throws Exception {

		int identificador = 0;
		String consoleOut = "";
		checkUsage(args);

		String streamName = args[0];
		String regionName = args[1];
		String jdkKey = args[2];
		Region region = RegionUtils.getRegion(regionName);
		if (region == null) {
			logger.error(regionName + " is not a valid AWS region.");
			throw new RuntimeException();
		}

		final AmazonKinesis kinesisClient = AmazonKinesisClientBuilder.standard().withRegion(regionName)
				.withClientConfiguration(ConfigurationUtils.getClientConfigWithUserAgent())
				.withCredentials(new ProfileCredentialsProvider("default")).build();

		// Validate that the stream exists and is active
		validateStream(kinesisClient, streamName);

		String exclusiveStartShardId = getShardId(streamName, kinesisClient);

		String shardIterator;
		GetShardIteratorRequest getShardIteratorRequest = new GetShardIteratorRequest();
		getShardIteratorRequest.setStreamName(streamName);
		getShardIteratorRequest.setShardId(exclusiveStartShardId);
		getShardIteratorRequest.setShardIteratorType("TRIM_HORIZON");

		GetShardIteratorResult getShardIteratorResult = kinesisClient.getShardIterator(getShardIteratorRequest);
		shardIterator = getShardIteratorResult.getShardIterator();

		List<Record> records;

		while (true) {
			// Create a new getRecordsRequest with an existing shardIterator
			// Set the maximum records to return to 25
			GetRecordsRequest getRecordsRequest = new GetRecordsRequest();
			getRecordsRequest.setShardIterator(shardIterator);
			getRecordsRequest.setLimit(25);

			GetRecordsResult result = kinesisClient.getRecords(getRecordsRequest);

			if (result == null) {
				System.out.println("GetRecordsResult es nulo");
			} 

			// Put the result into record list. The result can be empty.
			records = result.getRecords();

			if (records.isEmpty()) {
				if (identificador==0)
					System.out.println("Record is empty");
				else
					System.out.println("Stream reading finished");
				return consoleOut;
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException exception) {
				throw new RuntimeException(exception);
			}

			for (Iterator<Record> iterator = records.iterator(); iterator.hasNext();) {
				
				identificador++;
				Record record = (Record) iterator.next();
				ByteBuffer byteBuffer = record.getData();
				String recordData = StandardCharsets.UTF_8.decode(byteBuffer).toString();

				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.configure(MapperFeature.AUTO_DETECT_FIELDS, true);
				Event jdkStreamJSON = objectMapper.readValue(recordData, Event.class);

				if (jdkStreamJSON.getMetadata().getJUMPAPPKEY() != null && jdkStreamJSON.getMetadata().getJUMPAPPKEY().equals(jdkKey)){
					consoleOut = consoleOut + recordData + "\n\n";
				}
			}

			shardIterator = result.getNextShardIterator();

		}

	}

}

package com.amazonws.rest.kinesisDemo.service;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder;
import com.amazonaws.services.kinesis.model.PutRecordsRequest;
import com.amazonaws.services.kinesis.model.PutRecordsRequestEntry;
import com.amazonaws.services.kinesis.model.PutRecordsResult;
import com.amazonws.rest.kinesisDemo.utils.ConfigurationUtils;

@Component("kinesisProducer")
public class KinesisProducerTest {
	
	private static Logger logger = LoggerFactory.getLogger(KinesisProducerTest.class);

	public void createStream(String streamName, String regionName, String consoleIn) {
				
		final AmazonKinesis kinesisClient = AmazonKinesisClientBuilder.standard().withRegion(regionName)
				.withClientConfiguration(ConfigurationUtils.getClientConfigWithUserAgent())
				.withCredentials(new ProfileCredentialsProvider("default")).build();

		PutRecordsRequest putRecordsRequest = new PutRecordsRequest();
		List<PutRecordsRequestEntry> putRecordsRequestEntryList = new ArrayList<>();
		PutRecordsRequestEntry putRecordsRequestEntry = new PutRecordsRequestEntry();
		
		putRecordsRequest.setStreamName(streamName);
		//putRecordsRequestEntry.setData(ByteBuffer.wrap(dataTest.getBytes()));
		putRecordsRequestEntry.setData(ByteBuffer.wrap(consoleIn.getBytes()));
		putRecordsRequestEntry.setPartitionKey(String.format("partitionKey-%d", 1));
		putRecordsRequestEntryList.add(putRecordsRequestEntry);
		putRecordsRequest.setRecords(putRecordsRequestEntryList);
		PutRecordsResult putRecordsResult = kinesisClient.putRecords(putRecordsRequest);
		kinesisClient.putRecords(putRecordsRequest);
		logger.info("Put Result" + putRecordsResult);
	}
}

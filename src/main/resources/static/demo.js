$("#insertMessage").click(() => {
	$.ajax({
	 	 url: 'http://localhost:8000/kinesis/produce',
		 type: 'post',
		 data: $( "#consoleIn" ).val(),
		 contentType: 'application/json',
	 	 success: () => alert('Message generated')
 	 });
});

$("#showMessages").click(() => {
	 $.ajax({
		 url: 'http://localhost:8000/kinesis/consume',
		 type: 'post',
		 data: $( "#jumpKey" ).val(),
		 contentType: 'application/json',
		 success: data => $('#consoleOut').html(data)
	 });
});
